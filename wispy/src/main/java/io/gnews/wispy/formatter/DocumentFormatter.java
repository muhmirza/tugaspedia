package io.gnews.wispy.formatter;

import android.content.Context;

import io.gnews.wispy.Utils;
import io.gnews.wispy.WebPage;
import io.gnews.wispy.string.string;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.lang3.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * @author masasdani
 * Created Date Nov 24, 2015
 * 
 * used to generated formatted html from content extactor
 */
public class DocumentFormatter {

	private static final String TEMPLATE_FILENAME = "template.html";
	String htmlTemplate;
	
	public DocumentFormatter(Context context) {
		InputStream inputStream = null;
		try {
			inputStream = context.getAssets().open(TEMPLATE_FILENAME);
		} catch (IOException e) {
			e.printStackTrace();
		}
		htmlTemplate = Utils.read(inputStream);
	}
	
	/**
	 * generate formatted html content from template
	 * 
	 * @param webPage
	 * @return
	 */
	public String formatDocument(WebPage webPage){
		Document document = Jsoup.parse(htmlTemplate);
		setTitle(document, webPage.getTitle());
		setFavicon(document, webPage.getFaviconUrl());
		setContent(document, webPage.getContentHtml());
		setMainImage(document, webPage.getMainImageUrl());
		addPaddingToImgDiv(document);
		return document.html();
	}
	
	/**
	 * set title to template,
	 * if content already contain title, remove them
	 * 
	 * @param document
	 * @param title
	 */
	private void setTitle(Document document, String title){
		String escapedTitle = StringEscapeUtils.escapeHtml4(title);
		Elements titleElements = document.select("h1, h2, h3");
		for(Element element : titleElements){
			String escapedElementTitle = StringEscapeUtils.escapeHtml4(element.text());
			if(escapedElementTitle.trim().equals(escapedTitle.trim())){
				element.remove();
			}
		}
		Elements headerTitleElements = document.getElementsByTag("title");
		for(Element element : headerTitleElements){
			element.text(escapedTitle);
		}
		Element titleElement = document.getElementById("articleHeader-title");
		titleElement.text(title);
		
	}
	
	/**
	 * set generated content to template
	 * 
	 * @param document
	 * @param contentHtml
	 */
	private void setContent(Document document, String contentHtml){
		Element mainContentElement = document.getElementById("mainContent");
		mainContentElement.html(contentHtml);
	}

	/**
	 * set main image to template,
	 * if the extracted content doesn't have main image inside
	 * 
	 * @param document
	 * @param mainImageUrl
	 */
	private void setMainImage(Document document, String mainImageUrl){
		Element contentElement = document.getElementById("mainContent");
		Element mainImageElement = document.getElementById("mainImage");
		Elements imgElements = contentElement
				.getElementsByTag("img");
		boolean mainImageExist = false;
		for (Element imgElement : imgElements) {
			String imageUrl = imgElement.attr("src");
			if(!string.isNullOrEmpty(imageUrl)){
				mainImageExist = true;
				break;
			}
		}
		if(!mainImageExist && mainImageUrl != null){
			mainImageElement.attr("src", mainImageUrl);
		}else{
			mainImageElement.parent().remove();
		}
	}
	
	/**
	 * set favicon of this page
	 * 
	 * @param document
	 * @param faviconUrl
	 */
	private void setFavicon(Document document, String faviconUrl){
		Elements elements = document.select("link[rel=shortcut icon], link[rel=icon]");
		for (Element el : elements) {
			el.attr("href", faviconUrl);
		}
	}
	
	/**
	 * some of content has image containin in div, 
	 * add a image class to it to do css styling on it
	 * 
	 * @param document
	 */
	private void addPaddingToImgDiv(Document document){
		Element contentElement = document.getElementById("mainContent");
		Elements divElements = contentElement.select("div img");
		for(Element element : divElements){
			element.parent().addClass("image");
		}
	}
}
