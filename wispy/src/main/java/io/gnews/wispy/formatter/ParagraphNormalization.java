package io.gnews.wispy.formatter;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * @author masasdani
 * Created Date Dec 4, 2015
 * 
 * try to create parapraph normalization but still not work so far
 */
public class ParagraphNormalization {

	public static Element normalizeParagraph(Element topElement){
		Elements elements = topElement.getElementsByTag("p");
		for(Element e : elements){
			if(e.parent() != null){
				
			}else{
				
			}
		}
		return topElement;
	}
	
	public static void normalizeSiblingParagraph(Element topElement){
		Elements pelements = topElement.getElementsByTag("p");
		Elements siblings = pelements.get(0).siblingElements();
		StringBuilder stringBuilder = new StringBuilder();
//		stringBuilder.append("<p>\n");
		System.out.println("sibling found :" + siblings.size());
		List<String> tobeReplace = new ArrayList<String>();
		for(int i = 0 ; i < siblings.size() ; i++){
			Element sib = siblings.get(i);
			if(!sib.tag().toString().equals("p")){
				stringBuilder.append(sib.outerHtml());
//				System.out.println(i+ ":" + sib.tag()+ ":" +sib.outerHtml());
//				sib.remove();
			}else{
//				stringBuilder.append("</p>");
//				tobeReplace.add(stringBuilder.toString());
//				stringBuilder = new StringBuilder();
//				stringBuilder.append("<p>");
			}
//			System.out.println(i+ ": " + sib.tag()+ ": " +sib.outerHtml());
		}
//		stringBuilder.append("\n</p>");
//		System.out.println(stringBuilder.toString());
		tobeReplace.add(stringBuilder.toString());
		if(tobeReplace.size() > 0){
			String inner = topElement.html();
			inner.replaceAll(stringBuilder.toString(), "<p>\n"+stringBuilder.toString()+"\n</p>");
			topElement.html(inner);
		}
		System.out.println(topElement.html());
	}
	
	
}
