package io.gnews.wispy;

import java.util.Date;

/**
 * @author masasdani
 * Created Date Nov 25, 2015
 * 
 * class to get extraction from web page
 */
public class WebPage {

	private String title;
	private String faviconUrl;
	private String mainImageUrl;
	private Date publishDate;
	private String description;
	private String keywords;
	private String tags;
	private String content;
	private String contentHtml;
	private String rawHtml;
	private String cleanedHtml;
	private String nextUrl;
	private VideoData videoData;
	
	public WebPage() {
	
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFaviconUrl() {
		return faviconUrl;
	}

	public void setFaviconUrl(String faviconUrl) {
		this.faviconUrl = faviconUrl;
	}

	public String getMainImageUrl() {
		return mainImageUrl;
	}

	public void setMainImageUrl(String mainImageUrl) {
		this.mainImageUrl = mainImageUrl;
	}

	public Date getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getContentHtml() {
		return contentHtml;
	}

	public void setContentHtml(String contentHtml) {
		this.contentHtml = contentHtml;
	}

	public String getRawHtml() {
		return rawHtml;
	}

	public void setRawHtml(String rawHtml) {
		this.rawHtml = rawHtml;
	}

	public String getCleanedHtml() {
		return cleanedHtml;
	}

	public void setCleanedHtml(String cleanedHtml) {
		this.cleanedHtml = cleanedHtml;
	}

	public String getNextUrl() {
		return nextUrl;
	}

	public void setNextUrl(String nextUrl) {
		this.nextUrl = nextUrl;
	}

	public VideoData getVideoData() {
		return videoData;
	}

	public void setVideoData(VideoData videoData) {
		this.videoData = videoData;
	}
	
	@Override
	public String toString() {
		return getCleanedHtml();
	}
	
}
