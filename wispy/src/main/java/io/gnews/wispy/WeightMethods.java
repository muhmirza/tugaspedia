package io.gnews.wispy;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * original from Sreejith.S
 * 
 * @author masasdani
 * Created Date Nov 20, 2015
 * 
 * Some common weighted methods that can be used across algorithms.
 */
public class WeightMethods {

    /**
     *
     * @param element
     * @param s
     * @return
     */
    public static int getCharCount(Element element, char s) {
        return element.text().split(Character.toString(s)).length - 1;
    }

    /**
     *
     * @param linkNode
     * @return
     */
	public static double getLinkDensity(Element linkNode) {
		Elements linkElements = linkNode.getElementsByTag("a");
		int textLength = linkNode.text().length();
		if (textLength == 0)
			textLength = 1;
		int linkLength = 0;
		for (Element a : linkElements) {
			linkLength += a.text().length();

		}
		return linkLength / textLength;
	}

    /**
     *
     * @param element
     * @return
     */
    public static double getClassWeight(Element element) {
        double weight = 0;
        /* Look for a special classname */
        String className = element.className();
        if (!"".equals(className)) {
            if (Patterns.exists(Patterns.NEGATIVE, className)) {
                weight -= 25;
            }
            if (Patterns.exists(Patterns.POSITIVE, className)) {
                weight += 25;
            }
        }

        /* Look for a special ID */
        String id = element.id();
        if (!"".equals(id)) {
            if (Patterns.exists(Patterns.NEGATIVE, id)) {
                weight -= 25;
            }
            if (Patterns.exists(Patterns.POSITIVE, id)) {
                weight += 25;
            }
        }
        return weight;
    }

}
