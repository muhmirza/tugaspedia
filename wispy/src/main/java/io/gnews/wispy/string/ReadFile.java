package io.gnews.wispy.string;

import android.util.Log;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author   Sree
 *
 * Read a txt file line by line and add to a list
 */
public class ReadFile {

	private static BufferedReader bufferedReader;
	
	public static List<String> read(File fileName) {
		List<String> lines = new ArrayList<String>();
		FileInputStream fileInputStream = null;
		Log.d("wispy", "File created with name " + fileName.getName());
		try {
			fileInputStream = new FileInputStream(fileName);
		} catch (FileNotFoundException e) {
			Log.e("wispy", "Input file not found " + e.toString());
		}
		
		DataInputStream dataInputStream = new DataInputStream(fileInputStream);
		bufferedReader = new BufferedReader(new InputStreamReader(dataInputStream));
		String line;
		try {
			Log.d("wispy", "Reading line by line from file " + fileName.getAbsolutePath());
			while((line = bufferedReader.readLine()) != null)
			{
				lines.add(line);
			}
		} catch (IOException e) {
			Log.e("wispy", "IO Exception " + e.toString());
		}
		
		Log.d("wispy", "File read completely");
		return lines;
	}

}
