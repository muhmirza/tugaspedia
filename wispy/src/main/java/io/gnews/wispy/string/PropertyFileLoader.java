package io.gnews.wispy.string;

import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 
 * @author 		: Sreejith.S
 * 
 * A Property file loader class.
 *
 */
public class PropertyFileLoader {
	/** holds the properties */
	public Properties properties = new Properties(); 
	
	/** Name of the property file */
	private String propertyFileName = null;

	
	/**
	 * initialize the properties with the given property file name.
	 * @param fileName
	 */
	
	public void inilializeProperty(String fileName)
	{
		File propertyFile = new File(fileName);
		if(propertyFile.exists()) {
			Log.d("wispy", "Property file exists, " + propertyFile.getName());
			try {
				InputStream configStream = new FileInputStream(propertyFile);
				properties.load(configStream);
				setPropertyFileName(propertyFile.getName());
				
			}catch (FileNotFoundException e){
				Log.e("wispy", "Proeprty File Not found  " + e.toString());
				
			} catch (IOException e) {
				Log.e("wispy", "IO Exception  " + e.toString());
			}

			Log.d("wispy", "Property file loaded with " + propertyFile.getName());
			
		}
		else {
			Log.e("wispy", "Property file does not exists , exiting " + propertyFile.getAbsolutePath());
			properties = null;
			return;
		}
	}
	
	/** get the property file name */
	public String getPropertyFileName() {
		return propertyFileName;
	}
	
	/** Set the property file name 
	 * @return */
	public void setPropertyFileName(String fileName) {
		propertyFileName = fileName;
	}

}
