package io.gnews.wispy.string;

/**
 * original from Sreejith.S
 * 
 * @author masasdani
 * Created Date Nov 25, 2015
 */
public class string {
	
	private string(){
		
	}
	
	public static final String empty = "";
	
	public static final String[] emptyArray = new String[] {empty};
	
	public static boolean isNullOrEmpty(String input) {
		if (input == null) return true;
		if (input.length() == 0) return true;
		return false;
	}
	
	public static StringSplitter SPACE_SPLITTER = new StringSplitter(" ");
	
}

