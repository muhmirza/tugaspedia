package io.gnews.wispy.extractor;

import java.util.HashSet;
import java.util.Set;

import io.gnews.wispy.Patterns;
import io.gnews.wispy.string.string;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.jsoup.select.Selector;

/**
 * originally from Sreejith.S
 * 
 * @author masasdani
 * Created Date Dec 1, 2015
 */
public class MetaExtractor {

	/**
	 * if the article has meta keywords set in the source, use that
	 */
	public static String getMetaKeywords(Document doc) {
		return getMetaContent(doc, "meta[name=keywords]");
	}

	/**
	 * if the article has meta description set in the source, use that
	 */
	public static String getMetaDescription(Document doc) {
		return getMetaContent(doc, "meta[name=description]");
	}

	/**
	 * @param node
	 * @return
	 */
	public static Set<String> extractTags(Element node) {
		if (node.children().size() == 0)
			return Patterns.NO_STRINGS;
		Elements elements = Selector.select(Patterns.A_REL_TAG_SELECTOR, node);
		if (elements.size() == 0)
			return Patterns.NO_STRINGS;
		Set<String> tags = new HashSet<String>(elements.size());
		for (Element el : elements) {
			String tag = el.text();
			if (!string.isNullOrEmpty(tag))
				tags.add(tag);
		}
		return tags;
	}

	/**
	 * @param node
	 * @return
	 */
	public static String getFaviconUrl(Document document) {
		Elements elements = document.select("link[rel=shortcut icon], link[rel=icon]");
		String favicon = string.empty;
		for (Element el : elements) {
			favicon = el.attr("href");
			if(favicon.contains("favicon.ico")){
				return favicon;
			}
		}
		if(string.isNullOrEmpty(favicon)){
			favicon = "favicon.ico";
		}
		return favicon;
	}
	
	/**
	 * @param doc
	 * @param metaName
	 * @return
	 */
	private static String getMetaContent(Document doc, String metaName) {
		Elements meta = doc.select(metaName);
		if (meta.size() > 0) {
			String content = meta.first().attr("content");
			return string.isNullOrEmpty(content) ? string.empty : content.trim();
		}
		return string.empty;
	}
	
}
