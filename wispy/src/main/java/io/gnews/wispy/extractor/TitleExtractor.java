package io.gnews.wispy.extractor;

import android.util.Log;

import java.util.ArrayList;
import java.util.Collection;

import io.gnews.wispy.Patterns;
import io.gnews.wispy.string.StringSplitter;
import io.gnews.wispy.string.string;

import org.apache.commons.lang3.StringEscapeUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * originally from Sreejith.S
 *
 * @author masasdani
 * Created Date Nov 26, 2015
 *
 * find title on web document, first try get matches meta document title and content,
 * if not found try to select best content title
 */
public class TitleExtractor {

	/**
	 * find cleaned title from meta, 
	 * if empty or null, find best from element
	 *
	 * @param document
	 * @param topElement
	 * @return
	 */
	public static String getTitle(Document document, Element topElement){
		String title = getCleanedTitleFormMeta(document);
		if(string.isNullOrEmpty(title)){
			title = findBestTitleFromElement(topElement);
		}
		return title;
	}

	/**
	 * find cleaned title from meta, 
	 * if empty or null, find best from element
	 *
	 * @param document
	 * @return
	 */
	public static String getTitle(Document document){
		String title = getCleanedTitleFormMeta(document);
		if(string.isNullOrEmpty(title)){
			Element topElement = document.body();
			title = findBestTitleFromElement(topElement);
		}
		return title;
	}

	/**
	 * attemps to grab titles from the html pages, lots of sites use different
	 * delimiters for titles so we'll try and do our best guess.
	 *
	 * @param document
	 * @return
	 */
	private static String getCleanedTitleFormMeta(Document document) {
		String title = string.empty;

		try {
			Elements titleElem = document.getElementsByTag("title");
			if (titleElem == null || titleElem.isEmpty())
				return string.empty;

			String titleText = titleElem.first().text();

			if (string.isNullOrEmpty(titleText))
				return string.empty;

			boolean usedDelimeter = false;

			if (titleText.contains("|")) {
				titleText = doTitleSplits(titleText, Patterns.PIPE_SPLITTER);
				usedDelimeter = true;
			}
			if (titleText.contains("—")) {
				titleText = doTitleSplits(titleText, Patterns.MDASH_SPLITTER);
				usedDelimeter = true;
			}
			if (!usedDelimeter && titleText.contains("»")) {
				titleText = doTitleSplits(titleText, Patterns.ARROWS_SPLITTER);
				usedDelimeter = true;
			}
			if (!usedDelimeter && titleText.contains("-")) {
				titleText = doTitleSplits(titleText, Patterns.DASH_SPLITTER);
				usedDelimeter = true;
			}
			if (!usedDelimeter && titleText.contains(":")) {
				titleText = doTitleSplits(titleText, Patterns.COLON_SPLITTER);
			}

			// encode unicode charz
			title = StringEscapeUtils.escapeHtml4(titleText);
			title = Patterns.MOTLEY_REPLACEMENT.replaceAll(title);

		} catch (NullPointerException e) {
			Log.e("wispy", e.toString());
		}
		return StringEscapeUtils.unescapeHtml4(title);
	}

	/**
	 * based on a delimeter in the title take the longest piece or do some
	 * custom logic based on the site
	 *
	 * @param title
	 * @param splitter
	 * @return
	 */
	private static String doTitleSplits(String title, StringSplitter splitter) {
		int largetTextLen = 0;
		int largeTextIndex = 0;

		String[] titlePieces = splitter.split(title);

		// take the largest split
		for (int i = 0; i < titlePieces.length; i++) {
			String current = titlePieces[i];
			if (current.length() > largetTextLen) {
				largetTextLen = current.length();
				largeTextIndex = i;
			}
		}

		return Patterns.TITLE_REPLACEMENTS.replaceAll(titlePieces[largeTextIndex])
				.trim();
	}

	/**
	 * find largest text on collection, used to determine best title
	 *
	 * @param strings
	 * @return
	 */
	private static String findLargestStringOnCollection(Collection<String> strings){
		int largetTextLen = 0;
		String largestString = null;

		for(String s : strings){
			if(s.length() > largetTextLen){
				largetTextLen = s.length();
				largestString = s;
			}
		}
		return largestString;
	}

	/**
	 * find best title from content,
	 * using h1, h2, h3 and get the longest text
	 *
	 * @param topElement
	 * @return
	 */
	public static String findBestTitleFromElement(Element topElement){
		ArrayList<String> titleCandidates = new ArrayList<String>();

		Elements elements = topElement.getElementsByTag("h1");
		if(elements.size() == 0){
			elements = topElement.getElementsByTag("h2");
			if(elements.size() == 0){
				elements = topElement.getElementsByTag("h3");
			}
		}
		for (Element element : elements) {
			titleCandidates.add(element.text());
		}
		return findLargestStringOnCollection(titleCandidates);
	}
}