package io.gnews.wispy.extractor;

import android.util.Log;

import io.gnews.wispy.string.string;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * originally from Sreejith.S
 * 
 * @author masasdani
 * Created Date Nov 25, 2015
 * 
 * find best image on the web page, first try to find image on meta tag, then try to find the matches image in nodes.
 * if could not find the matches image, then try to compute the confident of image position
 */
public class ImageExtractor {

	private static final Matcher badImageMacther;
	private static final Matcher junkImageMacther;
	private Document document;
	private URL contextUrl;
	private String imageSrc;
	
	static {
		StringBuilder badImageBuilder = new StringBuilder();
		badImageBuilder.append(".html|.ico|button|twitter.jpg|facebook.jpg|digg.jpg|digg.png|delicious.png|facebook.png|reddit.jpg|doubleclick|diggthis|diggThis|adserver|/ads/|ec.atdmt.com");
		badImageBuilder.append("|mediaplex.com|adsatt|view.atdmt|reuters_fb_share.jpg");
		badImageBuilder.append("|twitter-login.png|google-plus.png");
		badImageBuilder.append("|gif$|gif\\?");
		badImageMacther = Pattern.compile(badImageBuilder.toString()).matcher(string.empty);
		
		StringBuilder junkImagesBuilder = new StringBuilder();
		junkImagesBuilder.append("d-logo-blue-100x100.png|WSJ_profile_lg.gif|dealbook75.gif|t_wb_75.gif|fivethirtyeight75.gif|current_issue.jpg|thecaucus75.gif");
		
		junkImageMacther = Pattern.compile(junkImagesBuilder.toString()).matcher(string.empty);
	}
	
	public String findBestImageURL(Document document, URL contextUrl){
		Element topElement = document.body();
		return findBestImageURL(document, topElement, contextUrl);
	}
	
	/**
	 * @param topElement
	 * @param document
	 * @return
	 */
	public String findBestImageURL(Document document, Element topElement, URL contextUrl){
		String bestImage = null;
		topElement = filterBadImages(topElement);
		Elements imgElements = topElement
				.getElementsByTag("img");
		ArrayList<String> imageCandidates = new ArrayList<String>();
		for (Element imgElement : imgElements) {
			String imgUrl = imgElement.attr("src");
			try {
				imgUrl = new URL(contextUrl, imgUrl).toString();
			} catch (MalformedURLException e) {
				Log.e("wispy",imgUrl, e);
			}
			imgElement.attr("src", imgUrl);
			imageCandidates.add(imgUrl);
		}
		Log.d("wispy", "Available size of images in top node : " + imageCandidates.size());
		
		// Setting the best image , in case still you need the top image.
		bestImage = getTopImage(document, contextUrl);
		Log.d("wispy", "BestImage : " + bestImage);
		
		if (string.isNullOrEmpty(bestImage)) { 
			if(imageCandidates.size() > 0){
				bestImage = imageCandidates.get(0);
			}
		}else{
			Log.d("wispy", "Best image found : " + bestImage);
			if(!imageCandidates.contains(bestImage)) {
				/** 
				 * removed because make duplication of image
				 */
				// addImageToTopOfContent(topElement, bestImage);
			}else {
				Log.d("wispy", "Top node already has the Best image found");
			}
		}
		return bestImage;
	}
	
	/**
	 * remove bad image in element
	 * 
	 * @param topNode
	 * @return
	 */
	private Element filterBadImages(Element topNode) {
		Elements topNodeImages = topNode.select("img");
		Log.d("wispy", "Found " + topNodeImages.size() + " images in top node");
		if (topNodeImages.size() > 0) {
			for (Element imageElement : topNodeImages) {
				String imgSrc = imageElement.attr("src");
				if (string.isNullOrEmpty(imgSrc))
					continue;
				badImageMacther.reset(imgSrc);
				if (badImageMacther.find()) {
					Log.d("wispy", "Found bad filename for image: " + imgSrc);
					imageElement.parent().remove();
				}
			}

		}
		return topNode;
	}

	/**
	 * @param doc
	 * @return
	 */
	private String getTopImage(Document doc, URL contextUrl) {
		this.document = doc;
		this.contextUrl = contextUrl;
		// fall back to meta tags, these can sometimes be inconsistent which is
		// why we favor them less
//		if (imageSrc == null)
		this.checkForMetaTag();

		return imageSrc;
	}

	/**
	 * checks to see if we were able to find open graph tags on this page
	 * 
	 * @return
	 */
	private boolean checkForMetaTag() {
		if (this.checkMetaTagsForImage("meta[property~=og:image]", "content",
				"opengraph")) {
			return true;
		}
		if (this.checkMetaTagsForImage("link[rel~=image_src]", "href",
				"linktag")) {
			return true;
		}
		return false;
	}

	/**
	 * checks to see if we were able to find open graph tags on this page
	 * 
	 * @return
	 */
	private boolean checkMetaTagsForImage(String metaRegex, String attr,
			String extractionType) {
		try {
			Elements metaElements = this.document.select(metaRegex);
			for (Element metaItem : metaElements) {
				if (metaItem.attr(attr).length() < 1) {
					Log.d("wispy", "Meta " + attr + " link faling");
					return false;
				}
				junkImageMacther.reset(metaItem.attr(attr));
				if(junkImageMacther.find()) {
					Log.d("wispy", "Known Junk image found in Meta , skipping " + metaItem.attr(attr));
					return false;
				}else {
					imageSrc = metaItem.attr(attr);
					Log.d("wispy", "Meta tag found and using : "
							+ this.imageSrc);
					imageSrc = new URL(contextUrl, imageSrc).toString();
					return true;
				}
			}
			return false;
		} catch (Exception e) {
			Log.e("wispy", e.toString(), e);
			return false;
		}
	}
	
	/**
	 * @param topElement
	 * @param bestImage
	 */
	@SuppressWarnings("unused")
	private void addImageToTopOfContent(Element topElement, String bestImage){
		Log.d("wispy", "Top node does not contain the same Best Image");
		try {
			Log.d("wispy", "Child Node : " + topElement.children().size());
			if(topElement.children().size() > 0) {
				Log.d("wispy", "Child Nodes greater than Zero " + topElement.children().size());
				topElement.child(0).before("<p><img src=" + bestImage + "></p>");
			} else {
				Log.d("wispy", "Top node has 0 childs appending after");
				topElement.append("<p><img src=" + bestImage + "></p>");
			}
		} catch (Exception e) {
			Log.e("wispy", e.toString(), e);
		}
	}
}
