package io.gnews.wispy;

/**
 * @author masasdani
 * Created Date Nov 25, 2015
 */
public class VideoData {

	private String url;
	private String thumbnail;
	private int duration;
	
	public VideoData() {
	
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}
	
}
