package io.gnews.wispy;

import android.content.Context;

import java.net.URL;

import io.gnews.wispy.cleaner.DocumentCleaner;
import io.gnews.wispy.extractor.ContentExtractor;
import io.gnews.wispy.extractor.ImageExtractor;
import io.gnews.wispy.extractor.MetaExtractor;
import io.gnews.wispy.extractor.TitleExtractor;
import io.gnews.wispy.extractor.VideoExtractor;
import io.gnews.wispy.formatter.DocumentFormatter;

import org.apache.commons.lang3.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * @author masasdani
 * Created Date Dec 4, 2015
 * 
 * class to do web page extraction of from url or raw html
 */
public class Wispy {

	public static final int MAX_CONNECTION_TIMEOUT = 30000;
	public static final String USER_AGENT = "Mozilla/5.0 (Linux; Android 4.4; Nexus 5 Build/_BuildID_) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36";

	public Context context;

	public Wispy(Context context){
		this.context = context;
	}

	/**
	 * get parsed web from given url and timeout request time
	 * 
	 * @param url
	 * @param timeout
	 * @return
	 */
	public WebPage extractWebPage(String url, int timeout) {
		WebPage webPage = new WebPage();
		try {
			Document document = Jsoup.connect(url).userAgent(USER_AGENT).timeout(timeout).get();
			return extractWebPage(url, document);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return webPage;
	}

	/**
	 * parsed raw html from text
	 * 
	 * @param rawHtml
	 * @return
	 */
	public WebPage extractWebPage(String url, String rawHtml){
		WebPage webPage = new WebPage();
		try {
			Document document = Jsoup.parse(rawHtml);
			return extractWebPage(url, document);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return webPage;
	}
	
	/**
	 * get parsed web page based on jsoup document
	 * 
	 * @param document
	 * @return
	 */
	public WebPage extractWebPage(String url, Document document) {
		WebPage webPage = new WebPage();
		try {
			URL contextUrl = new URL(url);
			
			/**
			 * do document cleaner
			 */
			DocumentCleaner cleaner = new DocumentCleaner();
			document = cleaner.clean(document);

			/**
			 * do content extractor
			 */
			ContentExtractor contentExtractor = new ContentExtractor();
			Element topElement = contentExtractor.fetchArticleContent(document);
			

			/**
			 * do image fetcher
			 */
			ImageExtractor imageFetcher = new ImageExtractor();
			String imageUrl = imageFetcher.findBestImageURL(document, topElement, contextUrl);

			/**
			 * do video fetcher
			 */
			VideoExtractor videoFetcher = new VideoExtractor();
			VideoData videoData = videoFetcher.extractVideo(document,
					topElement);
			
			/**
			 * get favicon and convert to absoluteUrl
			 */
			String favicon = MetaExtractor.getFaviconUrl(document);
			favicon = new URL(contextUrl, favicon).toString();
					
			webPage.setRawHtml(document.html());
			webPage.setTitle(TitleExtractor.getTitle(document, topElement));
			webPage.setFaviconUrl(favicon);
			webPage.setKeywords(MetaExtractor.getMetaKeywords(document));
			webPage.setDescription(MetaExtractor.getMetaDescription(document));
			webPage.setContentHtml(topElement.html());
			webPage.setContent(StringEscapeUtils.unescapeHtml4(topElement.text()));
			webPage.setMainImageUrl(imageUrl);
			webPage.setVideoData(videoData);
			webPage.setNextUrl(null);
			webPage.setPublishDate(null);
			webPage.setCleanedHtml(null);

			DocumentFormatter formatter = new DocumentFormatter(context);
			webPage.setCleanedHtml(formatter.formatDocument(webPage));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return webPage;
	}
}
