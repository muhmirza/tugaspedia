package io.gnews.wispy.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.webkit.WebView;

import java.util.ArrayList;
import java.util.List;

import io.gnews.wispy.WebPage;
import io.gnews.wispy.Wispy;

/**
 * Created by masasdani on 12/10/15.
 */
public class WispyWebView extends WebView {

    private String url;

    private String originalUrl;

    private WebPage webPage;

    private int progress = 0;

    private Context context;

    private int step = -1;

    private List<String> urlHistory;

    private boolean cleanHtml = true;

    public WispyWebView(Context context) {
        super(context);
        urlHistory = new ArrayList<>();
        this.context = context;
    }

    public WispyWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        urlHistory = new ArrayList<>();
        this.context = context;
    }

    public WispyWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        urlHistory = new ArrayList<>();
        this.context = context;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public WispyWebView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        urlHistory = new ArrayList<>();
        this.context = context;
    }

    @Override
    public String getOriginalUrl() {
        return originalUrl;
    }

    public void setOriginalUrl(String originalUrl) {
        this.originalUrl = originalUrl;
    }

    @Override
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public boolean canGoBack() {
        return step > 0;
    }

    @Override
    public boolean canGoForward() {
        return urlHistory.size() > 1 && (step + 1) != urlHistory.size();
    }

    @Override
    public void loadUrl(String url) {
        urlChange(url);
        new WispyURLLoader().execute(step);
    }

    public boolean isCleanHtml() {
        return cleanHtml;
    }

    public void setCleanHtml(boolean cleanHtml) {
        this.cleanHtml = cleanHtml;
    }

    @Override
    public void reload() {
        if (cleanHtml)
        loadDataWithBaseURL(getUrl(), webPage.getCleanedHtml(), "text/html", "UTF-8", getOriginalUrl());
        else
            loadDataWithBaseURL(getUrl(), webPage.getRawHtml(), "text/html", "UTF-8", getOriginalUrl());
    }

    @Override
    public void goBack() {
        step--;
        new WispyURLLoader().execute(step);
    }

    @Override
    public void goForward() {
        step++;
        new WispyURLLoader().execute(step);
    }

    @Override
    public int getProgress() {
        return progress;
    }

    public void loadUrlOld(String url){
        super.loadUrl(url);
    }

    @Override
    public void loadDataWithBaseURL(String baseUrl, String data, String mimeType, String encoding, String historyUrl) {
        super.loadDataWithBaseURL(baseUrl, data, mimeType, encoding, historyUrl);
    }

    private class WispyURLLoader extends AsyncTask<Integer, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = 0;
        }

        @Override
        protected String doInBackground(Integer... params) {
            publishProgress("5");
            url = urlHistory.get(params[0]);
            Wispy wispy = new Wispy(context);
            webPage =  wispy.extractWebPage(url, 30000);
            publishProgress("20");
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            progress = Integer.parseInt(values[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            loadDataWithBaseURL(url, webPage.getCleanedHtml(), "text/html", "UTF-8", url);
        }
    }

    private void urlChange(String url){
        step ++;
        urlHistory = urlHistory.subList(0, step);
        urlHistory.add(url);
    }

}
